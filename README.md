# Entrega do Teste: Seox front-end

## Intruções

- Baixar/clonar o repositório
- Abrir o arquivo public/index.html
- Pronto! 😃

## Considerações

1. Foi utilizado, como framework, apenas o arquivo de grids do Bootstrap;
2. Foram usadas funções do Sass para otimização do processo de criação das classes;
